import java.util.Scanner;

public class Soal9 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = 0;

        System.out.print("N = ");
        n = input.nextInt();

        int awal = n;
        int plus = n;
        for (int i = 1; i <= n; i++) {
            System.out.print(awal + " ");
            awal = awal + plus;
        }

        input.close();
    }
}
