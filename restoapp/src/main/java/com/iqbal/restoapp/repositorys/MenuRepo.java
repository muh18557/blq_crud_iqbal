package com.iqbal.restoapp.repositorys;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.iqbal.restoapp.models.MenuModel;
import jakarta.transaction.Transactional;

@Transactional

public interface MenuRepo extends JpaRepository<MenuModel, Long> {

    @Query(value = "select * from m_menu where is_delete = false  order by id ", nativeQuery = true)
    List<MenuModel> getMenu();

    @Query(value = "select * from m_menu where id = :id and is_delete='false'", nativeQuery = true)
    MenuModel getById(long id);

    // add data
    @Modifying // jika query merubah data tambahkan anotation berikut
    @Query(value = "insert into m_menu (nama,qty,harga) values(:nama,:qty,:harga)", nativeQuery = true)
    void add(String nama, int qty, int harga);

    // edit data
    @Modifying
    @Query(value = "update m_menu set nama=:nama, qty=:qty, harga=:harga, modified_on=now() where id=:id", nativeQuery = true)
    void edit(String nama, int qty, int harga, long id);

    // softdelete
    @Modifying
    @Query(value = "update m_menu set is_delete=true,deleted_on=now() where id = :id", nativeQuery = true)
    void del(long id);

}
