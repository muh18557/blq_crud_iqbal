import java.util.Scanner;

public class Soal11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String nama = "";

        System.out.print("Nama: ");
        nama = input.nextLine();

        // menggunakan substring
        String arrKata[] = nama.split(""); // pecah kalimat dlm kata array
        String kata = "";
        for (int i = arrKata.length - 1; i >= 0; i--) {
            kata = arrKata[i].substring(0, 1);
            String bintang = "";
            for (int j = 0; j < arrKata[i].length(); j++) {
                if (arrKata.length % 2 == 1) {
                    bintang = bintang + "**";
                } else {
                    bintang = bintang + "***";
                }
            }
            System.out.println(bintang + kata + bintang);
        }
        input.close();
    }
}
