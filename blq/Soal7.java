public class Soal7 {
    public static void main(String[] args) {
        int deret[] = { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };
        int max_num = deret[0];
        int min_num = deret[0];
        float jumlah = 0;
        float mean = 0;
        for (int i = 0; i < deret.length; i++) {
            if (deret[i] < min_num) {
                min_num = deret[i];
            }
        }
        for (int i = 0; i < deret.length; i++) {
            if (deret[i] > max_num) {
                max_num = deret[i];
            }
        }
        for (int i = 0; i < deret.length; i++) {
            jumlah = jumlah + deret[i];
        }

        mean = jumlah / (float) deret.length;

        System.out.println("Kecil : " + min_num);
        System.out.println("Besar : " + max_num);
        System.out.print("Mean  : ");
        System.out.printf("%.1f", mean);
    }
}
