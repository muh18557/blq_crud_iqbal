public class Soal22 {
    public static void main(String[] args) {
        int deret[] = { 1, 1, 2, 3, 5, 8, 13 };
        double panjangLilin[] = { 3, 3, 9, 6, 7, 8, 23 };
        System.out.print("Panjang Awal Lilin : ");
        for (int i = 0; i < panjangLilin.length; i++) {
            System.out.print(panjangLilin[i] + " ");
        }

        double waktuLeleh = 0;
        double fastest = Double.MAX_VALUE;
        int index = 0;

        for (int i = 0; i < panjangLilin.length; i++) {
            waktuLeleh = (double) panjangLilin[i] / deret[i];
            if (waktuLeleh < fastest) {
                fastest = waktuLeleh;
                index = i;
            }
        }
        System.out.println();
        System.out.println("Lilin yang paling pertama habis meleleh adalah lilin dengan panjang " + panjangLilin[index]
                + " meleleh dalam waktu " + (int) fastest + " detik");
    }
}
