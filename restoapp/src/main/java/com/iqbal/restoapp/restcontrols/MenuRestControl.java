package com.iqbal.restoapp.restcontrols;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import jakarta.validation.Valid;
import com.iqbal.restoapp.models.MenuModel;
import com.iqbal.restoapp.repositorys.MenuRepo;

@RestController
@RequestMapping("api/menu")
@CrossOrigin("*")
public class MenuRestControl {

    @Autowired
    MenuRepo repo;

    @GetMapping("/")
    public List<MenuModel> getMenu() {
        return repo.getMenu();
    }

    @GetMapping("/byid/{id}")
    public MenuModel getById(@PathVariable long id) {
        return repo.getById(id);
    }

    @PostMapping("/add")
    public Map<String, Object> Add(@Valid @RequestBody MenuModel d, BindingResult br) {
        Map<String, Object> hasil = new HashMap<>();
        if (br.hasErrors()) {
            StringBuilder b = new StringBuilder();
            List<FieldError> error = br.getFieldErrors();
            for (FieldError fieldError : error) {
                b.append(fieldError.getField() + ":" + fieldError.getDefaultMessage());
            }
            hasil.put("hasil", b.toString());
            return hasil;
        }
        try {
            repo.add(d.getNama(), d.getQty(), d.getHarga());
            hasil.put("hasil", "Add Success");
            return hasil;
        } catch (DataIntegrityViolationException e) {
            hasil.put("hasil", "Nama tidak boleh kembar");
            return hasil;
        }
    }

    @PutMapping("/edit/{id}")
    public Map<String, Object> edit(@Valid @PathVariable long id, @RequestBody MenuModel d) {
        Map<String, Object> result = new HashMap<>();
        try {
            repo.edit(d.getNama(), d.getQty(), d.getHarga(), id);
            result.put("hasil", "edit success");
            return result;
        } catch (DataIntegrityViolationException e) {
            result.put("hasil", "Data tidak boleh kembar !");
            return result;
        }
    }

    @DeleteMapping("/delete/{id}")
    public void del(@PathVariable long id) {
        repo.del(id);
    }
}
