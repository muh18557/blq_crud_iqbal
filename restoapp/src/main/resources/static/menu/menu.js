$(document).ready(function () {

    var pstatus = $('#pstatus').val()
    if (pstatus == 'menuList') {
        isiTable()
    } else if (pstatus == 'menuEdit') {
        editMenu()
    } else if (pstatus == 'menuDelete') {
        DelMenu()
    }

    $('#refreshdt').click(function () {
        isiTable()
    })

    function isiTable() {
        $.ajax({
            url: 'http://localhost/api/menu/',
            type: 'GET',
            success: function (data) {
                var txt = ''
                for (let i = 0; i < data.length; i++) {
                    txt += '<tr>'
                    txt += '<td>'
                    txt += data[i].id
                    txt += '</td>'
                    txt += '<td>'
                    txt += data[i].nama
                    txt += '</td>'
                    txt += '<td>'
                    txt += data[i].harga
                    txt += '</td>'
                    txt += '<td>'
                    txt += data[i].qty
                    txt += '</td>'
                    txt += '<td>'
                    txt += '<button type="button" class="menuEd" name="' + data[i].id + '">'
                    txt += '<i class="fa-solid fa-pen-to-square"></i>'
                    txt += '</button>'
                    txt += ' <button type="button" class="menuDel" name="' + data[i].id + '">'
                    txt += '<i class="fa-solid fa-trash"></i>'
                    txt += '</button>'
                    txt += '</td>'
                    txt += '</tr>'
                }
                $('#menuTbl').html(txt)

                //click btn dgn class menuEd pada tbl
                $('.menuEd').click(function () {
                    editId = $(this).attr('name')
                    sessionStorage.setItem('editIds', editId)
                    bukaMenuEdit()
                })

                //click btn dgn class menuDel pada tbl
                $('.menuDel').click(function () {
                    DelId = $(this).attr('name')
                    sessionStorage.setItem('DelIds', DelId)
                    bukaMenuDel()
                })
            }
        })
    }

    //panggil view menu add
    $('#menuAdd').click(function () {
        $.ajax({
            url: 'http://localhost/menu/add',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#isiContent').html(data)
            }
        })
    })

    //commit add to db
    $('#menuAddBtn').click(function () {
        var obj = {}
        obj.nama = $('#nama').val()
        obj.harga = $('#harga').val()
        obj.qty = $('#qty').val()
        var dJson = JSON.stringify(obj)
        $.ajax({
            url: 'http://localhost/api/menu/add',
            type: 'POST',
            contentType: 'application/json',
            data: dJson,
            success: function (data) {
                alert(data.hasil)
                kembali()
            }
        })
    })

    //fungsi kembali
    function kembali() {
        $.ajax({
            url: 'http://localhost/',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#isiContent').html(data)
            }
        })
    }

    //btn batal
    $('#menuAddBtl').click(function () {
        $.ajax({
            url: 'http://localhost/',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                kembali()
            }
        })
    })

    //buka menu Edit
    function bukaMenuEdit() {
        $.ajax({
            url: 'http://localhost/menu/edit',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#isiContent').html(data)
            }
        })
    }

    //tampilkan value edit
    function editMenu() {
        ids = sessionStorage.getItem('editIds')
        $.ajax({
            url: 'http://localhost/api/menu/byid/' + ids,
            type: 'GET',
            success: function (data) {
                $('#id').val(data.id)
                $('#nama').val(data.nama)
                $('#harga').val(data.harga)
                $('#qty').val(data.qty)
            }
        })
    }

    //Commit edit to db
    $('#menuEditBtn').click(function () {
        ids = $('#id').val()
        var obj = {}
        obj.nama = $('#nama').val()
        obj.harga = $('#harga').val()
        obj.qty = $('#qty').val()
        var dJson = JSON.stringify(obj)
        $.ajax({
            url: 'http://localhost/api/menu/edit/' + ids,
            type: 'PUT',
            contentType: 'application/json',
            data: dJson,
            success: function (data) {
                alert('Edit Success')
                kembali()
            }
        })
    })

    //buka menu Delete
    function bukaMenuDel() {
        $.ajax({
            url: 'http://localhost:/menu/delete',
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                $('#isiContent').html(data)
            }
        })
    }

    //tampilkan value delete
    function DelMenu() {
        ids = sessionStorage.getItem('DelIds')
        $.ajax({
            url: 'http://localhost/api/menu/byid/' + ids,
            type: 'GET',
            success: function (data) {
                $('#id').val(data.id)
                $('#nama').val(data.nama)
                $('#harga').val(data.harga)
                $('#qty').val(data.qty)
            }
        })
    }

    //commit delete to db
    $('#menuDelBtn').click(function () {
        ids = sessionStorage.getItem('DelIds')
        var obj = {}
        obj.id = $('#id').val()
        var dJson = JSON.stringify(obj)
        $.ajax({
            url: 'http://localhost/api/menu/delete/' + ids,
            type: 'DELETE',
            contentType: 'application/json',
            data: dJson,
            success: function (data) {
                alert("delete success")
                kembali()
            }
        })
    })

})