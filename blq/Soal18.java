public class Soal18 {
    public static void main(String[] args) {

        int[] jam = { 9, 13, 15, 17 };
        int[] kalori = { 30, 20, 50, 80 };
        int mulaiWO = 18;
        int diff;
        double workoutTime = 0;

        for (int i = 0; i < jam.length; i++) {
            diff = mulaiWO - jam[i];
            workoutTime += 0.1 * kalori[i] * diff;
        }

        int drinkWater = 500;
        if (workoutTime > 30) {
            drinkWater += (int) (100 * Math.floor(workoutTime / 30));
        }

        System.out.println("Air yang akan diminum sepanjang olahraga: " + drinkWater + " cc");
    }
}
