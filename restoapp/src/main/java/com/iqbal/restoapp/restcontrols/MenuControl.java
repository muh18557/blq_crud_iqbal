package com.iqbal.restoapp.restcontrols;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuControl {

    @GetMapping("/menu")
    public String menu() {
        return "/menu/list";
    }

    @GetMapping("/menu/add")
    public String menuadd() {
        return "/menu/add";
    }

    @GetMapping("/menu/edit")
    public String menuedit() {
        return "/menu/edit";
    }

    @GetMapping("/menu/delete")
    public String menudelete() {
        return "/menu/delete";
    }
}
