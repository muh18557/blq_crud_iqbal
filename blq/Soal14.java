import java.util.Scanner;

public class Soal14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int deretAwal[] = { 3, 9, 0, 7, 1, 2, 4 };
        int x = 0;

        System.out.print("Deret : ");
        for (int i = 0; i < deretAwal.length; i++) {
            System.out.print(deretAwal[i] + " ");
        }
        System.out.println();
        System.out.print("N = ");
        x = input.nextInt();

        for (int j = 1; j <= x; j++) {
            int awal = deretAwal[0];
            for (int i = 0; i < deretAwal.length - 1; i++) {
                deretAwal[i] = deretAwal[i + 1];
            }

            System.out.println();
            deretAwal[deretAwal.length - 1] = awal;
            System.out.print("N" + j + " = ");

            for (int i = 0; i < deretAwal.length; i++) {
                System.out.print(deretAwal[i] + " ");
            }

        }
        input.close();
    }
}
