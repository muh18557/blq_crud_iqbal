public class Soal6 {
    // Fungsi untuk mengecek apakah sebuah kata adalah palindrome
    static boolean isPalindrome(String word) {
        int left = 0;
        int right = word.length() - 1;

        // Melakukan pengecekan karakter dari kiri dan kanan secara bersamaan
        while (left < right) {
            if (word.charAt(left) != word.charAt(right)) {
                return false; // Jika ada perbedaan karakter, bukan palindrome
            }
            left++;
            right--;
        }
        return true; // Jika tidak ditemukan perbedaan, merupakan palindrome
    }

    public static void main(String[] args) {
        String testWord = "katak"; // Ganti kata yang ingin diuji di sini
        if (isPalindrome(testWord.toLowerCase())) {
            System.out.println(testWord + " adalah sebuah palindrome.");
        } else {
            System.out.println(testWord + " bukan sebuah palindrome.");
        }
    }
}
