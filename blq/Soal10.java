import java.util.Scanner;

public class Soal10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String nama = "";

        System.out.print("Nama: ");
        nama = input.nextLine();

        // menggunakan substring
        String arrKata[] = nama.split(" "); // pecah kalimat dlm kata array
        String awal = "";
        String akhir = "";
        for (int i = 0; i < arrKata.length; i++) {
            awal = arrKata[i].substring(0, 1);
            akhir = arrKata[i].substring(arrKata[i].length() - 1);
            String bintang = "***";
            System.out.print(awal + bintang + akhir + " ");
        }
        input.close();
    }
}
