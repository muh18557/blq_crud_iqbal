import java.util.Scanner;

public class Soal19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String kalimat = " ";
        String hasil = "Merupakan Pangram";

        System.out.print("kata : ");
        kalimat = input.nextLine();

        kalimat = kalimat.toLowerCase();

        for (char huruf = 'a'; huruf <= 'z'; huruf++) {
            if (!kalimat.contains(String.valueOf(huruf))) {
                hasil = "Bukan pangram";
                break;
            }
        }

        System.out.println(hasil);
        input.close();
    }
}
