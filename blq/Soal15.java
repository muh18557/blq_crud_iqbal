import java.util.Scanner;

public class Soal15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String waktu = " ";

        System.out.print("Input waktu (AM/PM) : ");
        waktu = input.nextLine();

        int HH = Integer.parseInt(waktu.substring(0, 2));
        String AMPM = waktu.substring(9, 11);
        if (HH >= 1 && HH <= 12 && AMPM.equalsIgnoreCase("PM")) {
            if (HH >= 1 && HH < 12) {
                int hh = HH + 12;
                System.out.print("24Hour : ");
                System.out.print(hh);
                System.out.print(waktu.substring(2, 5));
                System.out.print(waktu.substring(5, 8));
            } else if (HH == 12) {
                System.out.print("24Hour : ");
                System.out.print("12");
                System.out.print(waktu.substring(2, 5));
                System.out.print(waktu.substring(5, 8));
            }
        } else if (HH >= 1 && HH <= 12 && AMPM.equalsIgnoreCase("AM")) {
            if (HH >= 1 && HH < 12) {
                System.out.print("24 Hour : ");
                if (HH < 10) {
                    System.out.print("0" + HH);
                } else {
                    System.out.print(HH);
                }
                System.out.print(waktu.substring(2, 5));
                System.out.print(waktu.substring(5, 8));
            } else if (HH == 12) {
                System.out.print("24Hour : ");
                System.out.print("00");
                System.out.print(waktu.substring(2, 5));
                System.out.print(waktu.substring(5, 8));
            }
        }
        input.close();
    }
}
