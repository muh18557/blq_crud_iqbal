package com.iqbal.restoapp.restcontrols;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultControl {
    @GetMapping("/")
    public String awal() {
        return "Dashboard";
    }
}
