package com.iqbal.restoapp.models;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "mMenu")
public class MenuModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100, unique = true) // unique data tidak boleh sama
    @NotNull(message = "nama tidak boleh null")
    @NotBlank(message = "nama tidak boleh kosong")
    private String nama;

    private int harga;

    private int qty;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createOn;

    private LocalDateTime modifiedOn;

    private LocalDateTime deletedOn;

    @Column(columnDefinition = "BOOLEAN DEFAULT false")
    private boolean isDelete;
}
