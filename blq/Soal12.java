public class Soal12 {
    public static void main(String[] args) {
        int deret[] = { 1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8 };

        System.out.print("Awal  : ");
        for (int i = 0; i < deret.length; i++) {
            System.out.print(deret[i] + " ");
        }

        System.out.print("\nAkhir : ");
        for (int j = 0; j < deret.length; j++) {
            for (int i = 0; i < deret.length - 1; i++) {
                int awal = deret[i];
                if (deret[i] > deret[i + 1]) {
                    deret[i] = deret[i + 1];
                    deret[i + 1] = awal;
                }
            }
        }

        for (int i = 0; i < deret.length; i++) {
            System.out.print(deret[i] + " ");
        }

    }
}
