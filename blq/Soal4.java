public class Soal4 {
    // Fungsi untuk mengecek apakah sebuah bilangan prima
    static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    // Fungsi untuk menampilkan n bilangan prima pertama
    static void displayPrimes(int n) {
        int count = 0;
        int num = 2; // Dimulai dari bilangan 2, bilangan prima pertama

        System.out.println("N bilangan prima pertama:");
        while (count < n) {
            if (isPrime(num)) {
                System.out.print(num + " ");
                count++;
            }
            num++;
        }
    }

    public static void main(String[] args) {
        int n = 3; // Ganti nilai n sesuai dengan jumlah bilangan prima yang ingin ditampilkan
        displayPrimes(n);
    }
}
